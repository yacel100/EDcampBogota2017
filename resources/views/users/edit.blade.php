@extends('layouts.master')

@section('title')
    Perfil
@endsection

@section('header')
    @include('partials.header')
    @include('partials.user_menu')
@endsection

@section('content')
    <main class="ProfilePage  u-afterFixed">
        <aside class="ProfilePage-aside">
            <figure class="ProfilePage-figure">
                <div class="ProfilePage-avatar">
                    @if($user->picture)
                        <img src="{{ Storage::url($user->picture) }}" alt="{{ $user->name }}" width="100">
                    @endif
                </div>
                <figcaption class="ProfilePage-name"><h2>{{ $user->name }}</h2></figcaption>
            </figure>
        </aside>
        <article class="ProfilePage-main  u-container">
            <header class="u-title">
                <h2>Mi Perfil</h2>
            </header>
            <form id="Ingresar-form" class="Ingresar-form" method="POST" action="{{ route('users.update', $user) }}" enctype="multipart/form-data" class="Form">
                {{ csrf_field() }}

                @if(Session::has('info'))
                    <div class="Form-message  u-alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </div>
                @endif

                {{-- Le dice a laravel que será una actualización de un registro existente --}}
                <input name="_method" type="hidden" value="PUT">

                <div class="Form-element">
                    <label for="name"><i class="fa fa-user"></i></label>
                    <input type="text" name="name" id="name" placeholder="Nombres y Apellidos" value="{{ $user->name }}" readonly>
                </div>
                <div class="Form-element">
                    <label for="email"><i class="fa fa-envelope-o"></i></label>
                    <input type="email" name="email" id="email" placeholder="Correo electrónico" value="{{ $user->email }}" readonly>
                </div>
                <div class="Form-element">
                    <label for="current_password"><i class="fa fa-lock"></i></label>
                    <input type="password" name="current_password" id="current_password" placeholder="Contraseña actual" required>
                </div>
                @if ($errors->has('current_password'))
                    <div class="Form-message  u-error">
                        <strong>{{ $errors->first('current_password') }}</strong>
                    </div>
                @endif
                <div class="u-flex-space-between">
                    <div class="Form-element">
                        <label for="new_password"><i class="fa fa-lock"></i></label>
                        <input type="password" name="new_password" id="new_password" placeholder="Contraseña">
                    </div>
                    @if ($errors->has('new_password'))
                        <div class="Form-message  u-error">
                            <strong>{{ $errors->first('new_password') }}</strong>
                        </div>
                    @endif
                    <div class="Form-element">
                        <label for="new_password_confirmation"><i class="fa fa-lock"></i></label>
                        <input type="password" name="new_password_confirmation" id="new_password_confirmation" placeholder="Confirmar nueva contraseña">
                    </div>
                </div>
                <div class="Form-element  u-flex-center">
                    <label for="picture">Cambiar foto</label>
                    <label class="fileContainer  u-small">
                        Examinar
                        <input type="file" name="picture" id="picture">
                    </label>
                </div>
                @if ($errors->has('picture'))
                    <div class="Form-message  u-error">
                            <strong>{{ $errors->first('picture') }}</strong>
                    </div>
                @endif
                <div class="Form-element  u-bg-success  u-lg-w35">
                    <input type="submit" value="Guardar Cambios">
                </div>
            </form>
            <header class="u-flex-space-between">
                <h2>Mis pagos</h2>
                <span class="u-text-right">
                    <a href="{{ route('subscriptions.subscribe') }}" class="u-button  u-bg-alert">Renovar Suscripción</a>
                </span>
            </header>
            <table class="pure-table pure-table-horizontal">
                <thead>
                    <tr>
                        <th>Monto</th>
                        <th>Medio de pago</th>
                        <th>Inicio</th>
                        <th>Vence</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($subscriptions as $subscription)
                        <tr>
                            <td>${{ $subscription->invoice->price }}</td>
                            <td>PayU</td>
                            <td>{{ $subscription->begins_at->format('Y-m-d') }}</td>
                            <td>{{ $subscription->ends_at->format('Y-m-d') }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="text-center">
                {!! $subscriptions->links() !!}
            </div>
        </article>
    </main>
@endsection
